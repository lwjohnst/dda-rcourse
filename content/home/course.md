---
title: 'Material'
widget: "custom"  # Do not modify this line!
weight: 7
type: "page"
---

## [Current version of course](https://r-cubed.rostools.org)

The course material is found as an online book at [r-cubed.rostools.org](https://r-cubed.rostools.org).

## Past versions of the course

### [Version 1, March 2019](https://v1--dda-rcourse.netlify.com/)

The first version of this course, called *Reproducible Quantitative Methods: Data
analysis workflow using R*, was taught in March, 2019 in Middelfart, Denmark.

The original R Markdown teaching material can be found in the [GitLab
repository]. Anyone is free to use, re-use, modify, and so on (as per the
[license](/license/)) as long as you properly attribute the work. Please see the
["How to cite"] section of the README in the repository.

[GitLab repository]: https://gitlab.com/lwjohnst/dda-rcourse/tree/v1.0.0
["How to cite"]: https://gitlab.com/lwjohnst/dda-rcourse#how-to-cite-the-material
