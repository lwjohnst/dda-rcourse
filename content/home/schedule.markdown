---
title: 'Schedule'
widget: "custom"  # Do not modify this line!
weight: 6
type: "page"
---

The workshop is structured as a series of participatory live-coding sessions
(instructor and learner coding together) interspersed with hands-on exercises
and group work, using either a practice dataset or some other real-world dataset.
There are some lectures given, mainly at the start and end of the workshop.
The official DDA program can be found in [this PDF](files/dda-program.pdf).


|Date and time |Session topic                                               |Type       |Instructor |
|:-------------|:-----------------------------------------------------------|:----------|:----------|
|**June 22**   |                                                            |           |           |
|9:30          |Arrival; coffee and snacks                                  |           |           |
|10:00         |Introduction to the course                                  |Lecture    |Luke       |
|10:30         |Management of R projects (with short coffee break)          |Code-along |Luke       |
|12:30         |Lunch                                                       |           |           |
|13:30         |Data management and wrangling                               |Code-along |Bettina    |
|14:30         |Coffee break and hotel check-in                             |           |           |
|15:00         |Finding and obtaining open datasets                         |Lecture    |Daniel     |
|15:30         |Data management and wrangling (with short break)            |Code-along |Bettina    |
|17:30         |End of day short survey                                     |           |           |
|17:45         |Free time                                                   |           |           |
|18:30         |Dinner                                                      |           |           |
|20:30         |Social activity in the basement hotel bar                   |           |           |
|**June 23**   |                                                            |           |           |
|7:00-8:30     |Breakfast                                                   |           |           |
|8:30          |Collaboration and teamwork in research                      |Lecture    |Daniel     |
|9:00          |Version control and collaborative practices                 |Code-along |Luke       |
|10:15         |Coffee break and snacks                                     |           |           |
|10:30         |Version control and collaborative practices                 |Code-along |Luke       |
|12:15         |Lunch                                                       |           |           |
|13:15         |Data visualization                                          |Code-along |Helene     |
|14:45         |Coffee break and snacks                                     |           |           |
|15:00         |Data visualization                                          |Code-along |Helene     |
|17:00         |End of day short survey                                     |           |           |
|17:15         |Free time                                                   |           |           |
|18:30         |Dinner                                                      |           |           |
|20:30         |Drinks and chats around a bonfire                           |           |           |
|**June 24**   |                                                            |           |           |
|7:00-8:30     |Breakfast and checkout                                      |           |           |
|8:30          |Research in the era of (ir)reproducibility and open science |Lecture    |Luke       |
|9:15          |Creating reproducible documents                             |Code-along |Luke       |
|10:15         |Coffee break and snacks                                     |           |           |
|10:30         |Creating reproducible documents                             |Code-along |Luke       |
|12:15         |Lunch                                                       |           |           |
|13:15         |Group work: Presentation of projects, and discussions       |           |           |
|15:15         |Closing remarks and short survey                            |           |           |
|15:30         |Farewell                                                    |           |           |
